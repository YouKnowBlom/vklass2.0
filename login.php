<?php 
session_start();
if(!isset($_SESSION['User'])){
	if(isset($_POST['username']) && isset($_POST['password'])){

	$servername ="localhost";
	$username ="root";
	$password ="";
	$dbname ="vklass2.0";
	
	//skapar connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	
	// kollar connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	//tar värdena ifrån formuläret
	$student = $conn->real_escape_string($_POST['username']);
	$password = $conn->real_escape_string($_POST['password']);
	
	//kollar om de värdena i frormuläret stämmer överens  med det i användaren i db:n
	$sql = 'SELECT * FROM students WHERE namn = "' . $student . '"';
	$result = $conn->query($sql);
	
	
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
		
		if(password_verify($password, $row["password"])){
			$_SESSION['User'] = $row["fullname"];
			$_SESSION['userid'] = $row["id"];
			$_SESSION['Username'] = $row["namn"];
			$_SESSION['classID'] = $row["class"];
			$_SESSION['userType'] = $row["type"];
			header("location: index.php");
		} else {
			$_SESSION['login_error'] = "Fel lösenord";
		}
	} else {
		$_SESSION['login_error'] = "Användaren existerar inte";
	}
	$conn->close();
}
} else {
	header("location: index.php");
}
include("login.html");

if(isset($_SESSION['login_error'])){
	echo '<p style="color:red;font-size:30px;">' . $_SESSION['login_error'] . '</p>';
	unset($_SESSION['login_error']);
}
?>
