<?php
	session_start(); // inloggning krävs för att komma åt sidan

	// kollar om session är satt, om ej så skickas man till logga in sidan.
	if(!isset($_SESSION['User'])){
		header("location:login.php"); // redirect till login.
	}
	else {
		//echo "Välkommen ". $_SESSION["User"];
	}	

	//include("index.html");

//gör hela html sidan till en string och sätter det i en variabel
$page = file_get_contents("index.html");

// kollar om användaren är en teacher
if($_SESSION["userType"] == 2){

	//om användaren är en teacher så sätts en "teacher"-div (bubbla) in istället för '<!--{{teacher}}-->' i html stringen
	$replacestring = "<div class=\"round\" id=\"teacher\"><h1>LÄRARE</h1></div>";
	$page = str_replace("{{teacherbubble}}",$replacestring , $page);
} else {
	//om användaren inte är en teacher så tas '{{teacher}}' bort ifrån html-koden ()
	$page = str_replace("{{teacherbubble}}","", $page);
}
//skriver ut sidan
echo $page;
?>
