<?php
	session_start();

	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "vklass2.0";
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	} 
	$conn->query("SET NAMES utf8");
	
	$username = $_SESSION['Username'];
	
	$sql = "SELECT students.id, students.namn, students.email, students.class, students.fullname, students.class AS 'classID', classes.name AS 'class' FROM students INNER JOIN classes ON students.class = classes.id WHERE namn='$username'";
	$result = $conn->query($sql);
	
	$user = array();
	if ($result->num_rows === 1) {
		$row = $result->fetch_assoc();
		
		$user = array("userId" => $row["id"], "username" => $row["namn"], "email" => $row["email"], "classID" => $row["classID"], "class" => $row["class"], "name" => $row["fullname"]);
		
		$userid = $user["userId"];
	} else {
		//echo "~0 results";
	}
	
	$sql = "SELECT attendance.type, attendance.week, lessons.day, lessons.id, courses.name FROM attendance INNER JOIN students ON attendance.student = students.id INNER JOIN lessons ON attendance.lesson = lessons.id INNER JOIN courses ON lessons.course=courses.id WHERE attendance.student='$userid' AND attendance.type=1 ORDER BY attendance.week, lessons.day";
	$result = $conn->query($sql);
	
	$invalidAttendence =[];
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$new = array("type" => $row["type"] * 1, "week" => $row["week"] * 1, "day" => $row["day"] * 1, "name" => $row["name"], "lessonID" => $row["id"]);
			
			array_push($invalidAttendence, $new);
		}
	} else {
		//echo "~0 results";
	}
	
	$sql = "SELECT attendance.type, attendance.week, lessons.day, lessons.id, courses.name FROM attendance INNER JOIN students ON attendance.student = students.id INNER JOIN lessons ON attendance.lesson = lessons.id INNER JOIN courses ON lessons.course=courses.id WHERE attendance.student='$userid' AND attendance.type=2 ORDER BY attendance.week, lessons.day";
	$result = $conn->query($sql);
	
	$validAttendence =[];
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$new = array("type" => $row["type"] * 1, "week" => $row["week"] * 1, "day" => $row["day"] * 1, "name" => $row["name"], "lessonID" => $row["id"]);
			
			array_push($validAttendence, $new);
		}
	} else {
		//echo "~0 results";
	}
	
	$class = $user["classID"];
	$sql = "select lessons.id, lessons.startTime, lessons.endTime FROM lessons WHERE lessons.class='$class'";
	$result = $conn->query($sql);
	
	$lessons =[];
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$new = array("id" => $row["id"], "start" => strtotime($row["startTime"]), "end" => strtotime($row["endTime"]), "length" => round((strtotime($row["endTime"]) - strtotime($row["startTime"])) / 60, 2));
			
			array_push($lessons, $new);
		}
	} else {
		//echo "~0 results";
	}
	
	$totalTime = 0;
	foreach($lessons as $lesson) {
		$totalTime += $lesson["length"];
	}
	$totalTime *= 4;
	
	$valid = 0;
	foreach($validAttendence as $lesson) {
		foreach($lessons as $value) {
			if($value["id"] == $lesson["lessonID"]) {
				$valid += $value["length"];
			}
		}
	}
	
	$invalid = 0;
	foreach($invalidAttendence as $lesson) {
		foreach($lessons as $value) {
			if($value["id"] == $lesson["lessonID"]) {
				$invalid += $value["length"];
			}
		}
	}
	$valid = round(($valid / $totalTime)*100, 1);
	$invalid = round(($invalid / $totalTime)*100, 1);
	
	$conn->close();
?>
<?php
	$invalidlessons = "";
	if(count($invalidAttendence)) {
		$count = 0;
		foreach($invalidAttendence as $value) {
			$count ++;
			if($count > 3) {
				break;
			}
			$week = $value["week"];
			$dto = new DateTime();
			$date = $dto->modify('+' . $value["day"] . ' days')->format('d/m-Y');
			if($value["type"] == 1){
				$invalidlessons .= "<li>" . $value['name'] . " $date</li>";
			}
		}
	} else {
		$invalidlessons .= "Tomt";
	}
?>
<?php
	$validlessons = "";
	if(count($invalidAttendence)) {
		$count = 0;
		foreach($validAttendence as $value) {
			$count ++;
			if($count > 3) {
				break;
			}
			$week = $value["week"];
			$dto = new DateTime();
			$date = $dto->modify('+' . $value["day"] . ' days')->format('d/m-Y');
			if($value["type"] == 2){
				$validlessons .= "<li>" . $value['name'] . " $date</li>";
			}
		}
	} else {
		$validlessons .= "Tomt";
	}
?>

<?php
	$page = file_get_contents("profile.html");

	$search  = array("{{username}}", "{{class}}", "{{attendance}}", "{{valid}}", "{{invalid}}", "{{invalidlessons}}", "{{validlessons}}");

	
    $replace = Array($user["name"], $user["class"], 100 - ($valid + $invalid), $valid, $invalid, $invalidlessons, $validlessons);

    $page = str_replace($search, $replace , $page);

    echo $page;
?>