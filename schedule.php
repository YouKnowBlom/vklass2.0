<?php session_start(); ?>
<?php
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "vklass2.0";
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	} 
	$conn->query("SET NAMES utf8");
	$classId = $_SESSION['classID'];
	$sql = "SELECT classes.id, classes.name, lessons.id AS 'lessonid', lessons.startTime, lessons.endTime, lessons.day, courses.name, courses.shortName, courses.teacher AS 'teacherid', students.fullname AS 'teacher', rooms.name AS 'room' FROM classes INNER JOIN class_courses ON class_courses.class = classes.id INNER JOIN courses ON class_courses.course = courses.id INNER JOIN lessons ON lessons.course = courses.id Inner JOIN students ON students.id = courses.teacher INNER JOIN rooms ON lessons.room = rooms.id WHERE classes.id='$classId'";
	$result = $conn->query($sql);

	$lessons =[[],[],[],[],[]];
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$class = array("id" => $row["id"], "name" => $row["name"]);
			$lessonid = $row["lessonid"];
			$day = $row["day"] - 1;

			$lesson = array("shortname" => $row["shortName"], "name" => $row["name"], "room" => $row["room"], "startTime" => substr($row["startTime"], 0, 5), "endTime" => substr($row["endTime"], 0, 5), "cancelled" => false, "id" => $lessonid, "teacherid" => $row["teacherid"], "teacher" => $row["teacher"]);

			$sql = "SELECT cancelledlessons.lessonid, cancelledlessons.week, cancelledlessons.cancelledby, students.fullname FROM cancelledlessons INNER JOIN students ON cancelledlessons.cancelledby = students.id WHERE lessonid=$lessonid";
			$result1 = $conn->query($sql);

			if ($result1->num_rows === 1) {
				$row1 = $result1->fetch_assoc();
				$lesson["cancelled"] = true;
				$lesson["cancelledBy"] = $row1["fullname"];
				$lesson["weekCancelled"] = $row1["week"];
			}


			array_push($lessons[$day], $lesson);
		}
	} else {
		//echo "~0 results";
	}
	$conn->close();
?>

<?php
	$days = ["", "", "", "", ""];
	for($i = 0; $i < 5; $i++) {
		foreach($lessons[$i] as &$lesson) {
			$id = $lesson["id"];
			$name = $lesson["shortname"];
			$fullname = $lesson["name"];
			$start = $lesson["startTime"];
			$end = $lesson["endTime"];
			$cancelled = $lesson["cancelled"];
			$teacherid = $lesson["teacherid"];
			$teacher = $lesson["teacher"];
			$top = ((intval(substr($start, 0, 2)) + intval(substr($start, 3, 2))/60) - 8) * 11;
			$height = ((intval(substr($end, 0, 2)) + intval(substr($end, 3, 2))/60) - (intval(substr($start, 0, 2)) + intval(substr($start, 3, 2))/60)) * 9;
			if($cancelled){
				$user = $lesson["cancelledBy"];
				$days[$i] .= "<div class='lesson cancelledLesson' style='height:$height%;top:$top%;' data-lessonid='$id' data-name='$fullname'  data-cancelledBy='$user' data-teacherid='$teacherid' data-teacher='$teacher'>";
			} else {
				$days[$i] .= "<div class='lesson' style='height:$height%;top:$top%;' data-lessonid='$id' data-name='$fullname'  data-teacherid='$teacherid' data-teacher='$teacher'>";
			}
			$days[$i] .= "<span class='name'>$name</span>
			<span class='startTime'>$start</span>
			<span class='room'>3612</span>
			<span class='endTime'>$end</span>
			</div>";
		}
	}
?>

<?php
	$page = file_get_contents("schedule.html");

	$search  = Array("{{week}}", "{{monday}}", "{{tuesday}}", "{{wednesday}}", "{{thursday}}", "{{friday}}", "{{cancellesson}}", "{{attendancebutton}}", "{{note}}");
	if($_SESSION["userType"] == 2){
		$button = "<div id='cancelButton'>Ställ in lektionen</div>";
	} else {
		$button = "";
	}
	if($_SESSION["userType"] == 2){
		$attendancebutton = "<a id='attendanceButton' href='./teacher.php?attendance=1&id=1&w=1'>Närvaro</a>";
	} else {
		$attendancebutton = "";
	}
	if($_SESSION["userType"] == 2){
		$note = "<textarea>Läs kapitel 3 &amp; 5.<br>Förhör på måndag.</textarea>";
	} else {
		$note = "Läs kapitel 3 &amp; 5.<br>Förhör på måndag.";
	}
    $replace = Array(date("W"), $days[0], $days[1], $days[2], $days[3], $days[4], $button, $attendancebutton, );

    $page = str_replace($search, $replace , $page);

    echo $page;
?>