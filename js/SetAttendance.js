$('#setAttendance input').on('change', function() {
	var s = "setAttendance.php?id=" + $("#setAttendance").data("lessonid") + "&w=" + $("#setAttendance").data("week");
	var inputs = $('input:checked', "#setAttendance");
	inputs.each(function() {
		if($(this).val() === "absent") {
			s += "&student" + $(this).attr("name") + "=2";
		} else if($(this).val() === "invalidAbsent") {
			s += "&student" + $(this).attr("name") + "=1";
		}
	});
	$("#saveAttendance").attr("href", s);
    //alert($('input[name=3]:checked', '#setAttendance').val()); 
});