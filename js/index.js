(function(){
    function mainPageButtons(){
        let profile = document.querySelector("#profil h1"),
        profileBubble = document.querySelector("#profil"),
        news = document.querySelector("#news h1"), 
        newsBubble = document.querySelector("#news"), 
        schedule = document.querySelector("#schedule h1"), 
        scheduleBubble = document.querySelector("#schedule"), 
        courses = document.querySelector("#courses h1"),
        coursesBubble = document.querySelector("#courses"),
        teacher = document.querySelector("#teacher h1"),
        teacherBubble = document.querySelector("#teacher"),
        animationDelay = 2;
        
        profile.addEventListener("click", function(e){
            profileBubble.style.animation = "bubbleExpand " + animationDelay + "s 1 forwards";
            setTimeout(function(){location.href ="profile.php";}, 2000)
        });
        
        news.addEventListener("click", function(e){
            newsBubble.style.animation = "bubbleExpand " + animationDelay + "s 1 forwards";
            setTimeout(function(){location.href ="news.php";}, 2000)
        });
        
        schedule.addEventListener("click", function(e){
            scheduleBubble.style.animation = "bubbleExpand " + animationDelay + "s 1 forwards";
            setTimeout(function(){location.href ="schedule.php";}, 2000)
        });
        
        courses.addEventListener("click", function(e){
            coursesBubble.style.animation = "bubbleExpand " + animationDelay + "s 1 forwards";
            setTimeout(function(){location.href ="courses.php";}, 2000)
        });
        if(document.querySelector("#teacher")){
            teacher.addEventListener("click", function(e){
                teacherBubble.style.animation = "bubbleExpand " + animationDelay + "s 1 forwards";
                setTimeout(function(){location.href ="teacher.php";}, 2000)
            });
        }
    }
    function bubbleBackAnimation(){
        let url = location.href, urlParts = url.split("?"), reverseAnimationDelay = 2;
        
        urlParts.forEach(element => {
            if(element.search(/from=[A-Za-z]+/g) > -1){
                switch(element.substring(5, element.length)){
                    case "profile":
                    let profileBubble = document.querySelector("#profil");
                    profileBubble.style.animation = "bubbleExpand " + reverseAnimationDelay + "s 1 reverse";
                    history.replaceState({}, "", "index.php");
                    
                    setTimeout(function(){
                        profileBubble.style.animation = "";
                    }, reverseAnimationDelay * 1000)
                    
                    break;
                    case "news":
                    newsBubble = document.querySelector("#news");
                    newsBubble.style.animation = "bubbleExpand " + reverseAnimationDelay + "s 1 reverse";
                    history.replaceState({}, "", "index.php");
                    
                    setTimeout(function(){
                        newsBubble.style.animation = "";
                    }, reverseAnimationDelay * 1000)
                    break;
                    case "schedule":
                    let scheduleBubble = document.querySelector("#schedule");
                    scheduleBubble.style.animation = "bubbleExpand " + reverseAnimationDelay + "s 1 reverse";
                    history.replaceState({}, "", "index.php");
                    
                    setTimeout(function(){
                        scheduleBubble.style.animation = "";
                    }, reverseAnimationDelay * 1000)
                    break;
                    case "courses":
                    let coursesBubble = document.querySelector("#courses");
                    coursesBubble.style.animation = "bubbleExpand " + reverseAnimationDelay + "s 1 reverse";
                    history.replaceState({}, "", "index.php");
                    
                    setTimeout(function(){
                        coursesBubble.style.animation = "";
                    }, reverseAnimationDelay * 1000)
                    break;
                    
                    case "teacher":
                    if(document.querySelector("#teacher")){
                        let teacherBubble = document.querySelector("#teacher");
                        teacherBubble.style.animation = "bubbleExpand " + reverseAnimationDelay + "s 1 reverse";
                        history.replaceState({}, "", "index.php");
                        
                        setTimeout(function(){
                            teacherBubble.style.animation = "";
                        }, reverseAnimationDelay * 1000)
                    }
                    break;
                }
            }
        });
    }
    
    function init(){
        window.addEventListener("DOMContentLoaded", function(e){
            mainPageButtons();
            bubbleBackAnimation();
        });
    }
    init();
}())