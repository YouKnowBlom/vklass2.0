var scrolldiv = document.getElementById("fingscrolldiv");

var d = new Date();
scrolldiv.scrollTo((d.getDay() - 1) * Math.max(document.documentElement.clientWidth, window.innerWidth || 0), 0);

var timeOutVar, secondTimeout;
scrolldiv.addEventListener("scroll", function(){
	
	clearTimeout(timeOutVar);
	clearTimeout(secondTimeout);
	
	timeOutVar = setTimeout(function(){
		var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		var x = scrolldiv.scrollLeft;

		if (x < w * 1 - w / 2) {
			myScrollTo(0, "måndag");
		} else if (x < w * 2 - w / 2) {
			myScrollTo(w * 1, "tisdag");
		} else if (x < w * 3 - w / 2) {
			myScrollTo(w * 2, "onsdag");
		} else if (x < w * 4 - w / 2) {
			myScrollTo(w * 3, "torsdag");
		} else if (x < w * 5 - w / 2) {
			myScrollTo(w * 4, "fredag");
		}
	}, 1);
});

function myScrollTo(x, calledFrom){
	var a = new Date();
	
	var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	var scrollX = scrolldiv.scrollLeft;
	
	if (scrollX < x) {
		scrolldiv.scrollTo(scrollX + 1, 0);
	} else if (scrollX > x) {
		scrolldiv.scrollTo(scrollX - 1, 0);
	}
	
	if (scrollX !== x) {
		secondTimeout = setTimeout(() =>{
			myScrollTo(x);
		}, 1);
	}
}

var id = 0;
$(".lesson").click(function(){
	id = $(this).data("lessonid");
	$("#dialog").css("display", "block");
	$("#lessonName").html($(this).data("name"));
	$("#attendanceButton").attr("href", $("#attendanceButton").attr("href").substring(0, $("#attendanceButton").attr("href").indexOf('&') + 1) + "id=" + id + "&w=" + week);
	if($(this).hasClass("cancelledLesson")) {
		$("#cancelledBy h3").html("Inställd av: " + $(this).data("cancelledby"));
		$("#cancelButton").css("background-color", "lime");
		$("#cancelButton").html("Oställ in lektionen");
	} else {
		$("#cancelledBy h3").html("");
		$("#cancelButton").css("background-color", "yellow");
		$("#cancelButton").html("Ställ in lektionen");
	}
});
$("#close").click(function(){
	$("#dialog").css("display", "none");
});

$("#cancelButton").click(function(){
	window.location.replace("cancellesson.php?id=" + id + "&w=" + week);
});

var week = 0;
$(document).ready(function() {
	week = $("#week").text() * 1;
});