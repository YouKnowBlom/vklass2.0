-- --------------------------------------------------------
-- Värd:                         127.0.0.1
-- Server version:               10.1.26-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for vklass2.0
CREATE DATABASE IF NOT EXISTS `vklass2.0` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `vklass2.0`;

-- Dumping structure for tabell vklass2.0.classes
CREATE TABLE IF NOT EXISTS `classes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `lessons` text NOT NULL,
  UNIQUE KEY `name` (`name`(100)),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumpar data för tabell vklass2.0.classes: ~1 rows (approximately)
DELETE FROM `classes`;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
INSERT INTO `classes` (`id`, `name`, `lessons`) VALUES
	(1, 'TE417', '[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]');
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;

-- Dumping structure for tabell vklass2.0.courses
CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  UNIQUE KEY `name` (`name`(100)),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumpar data för tabell vklass2.0.courses: ~9 rows (approximately)
DELETE FROM `courses`;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` (`id`, `name`) VALUES
	(1, 'Datalagring'),
	(2, 'Examensarbete VI'),
	(3, 'Gränssnittsdesign'),
	(4, 'Gymnasieingenjören i praktiken'),
	(5, 'Mentorstid'),
	(6, 'Mjukvarudesign'),
	(7, 'Mobila applikationer'),
	(8, 'Programmering 2'),
	(9, 'Webbutveckling 2');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;

-- Dumping structure for tabell vklass2.0.days
CREATE TABLE IF NOT EXISTS `days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumpar data för tabell vklass2.0.days: ~5 rows (approximately)
DELETE FROM `days`;
/*!40000 ALTER TABLE `days` DISABLE KEYS */;
INSERT INTO `days` (`id`) VALUES
	(1),
	(2),
	(3),
	(4),
	(5);
/*!40000 ALTER TABLE `days` ENABLE KEYS */;

-- Dumping structure for tabell vklass2.0.lessons
CREATE TABLE IF NOT EXISTS `lessons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course` int(11) NOT NULL,
  `room` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `startTime` time NOT NULL DEFAULT '00:00:00',
  `endTime` time NOT NULL DEFAULT '00:00:00',
  KEY `id` (`id`),
  KEY `FK_lession_courses` (`course`),
  KEY `FK_lession_rooms` (`room`),
  KEY `FK_lessons_days` (`day`),
  CONSTRAINT `FK_lession_courses` FOREIGN KEY (`course`) REFERENCES `courses` (`id`),
  CONSTRAINT `FK_lession_rooms` FOREIGN KEY (`room`) REFERENCES `rooms` (`id`),
  CONSTRAINT `FK_lessons_days` FOREIGN KEY (`day`) REFERENCES `days` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Dumpar data för tabell vklass2.0.lessons: ~18 rows (approximately)
DELETE FROM `lessons`;
/*!40000 ALTER TABLE `lessons` DISABLE KEYS */;
INSERT INTO `lessons` (`id`, `course`, `room`, `day`, `startTime`, `endTime`) VALUES
	(1, 8, 1, 1, '08:20:00', '09:20:00'),
	(2, 4, 1, 1, '10:00:00', '10:55:00'),
	(3, 7, 1, 1, '11:05:00', '12:00:00'),
	(5, 6, 1, 1, '12:40:00', '13:35:00'),
	(6, 9, 1, 1, '14:00:00', '14:55:00'),
	(7, 2, 1, 2, '09:40:00', '10:35:00'),
	(8, 7, 1, 2, '11:15:00', '12:15:00'),
	(9, 5, 1, 2, '13:00:00', '13:30:00'),
	(10, 3, 1, 2, '13:40:00', '14:40:00'),
	(11, 1, 1, 2, '14:50:00', '15:45:00'),
	(12, 6, 1, 3, '08:20:00', '09:20:00'),
	(14, 2, 1, 3, '09:30:00', '10:30:00'),
	(15, 3, 1, 3, '10:40:00', '11:35:00'),
	(16, 1, 1, 4, '08:20:00', '09:20:00'),
	(17, 4, 1, 4, '11:45:00', '12:45:00'),
	(18, 4, 1, 5, '08:20:00', '09:15:00'),
	(19, 8, 1, 5, '11:10:00', '12:05:00'),
	(20, 9, 1, 5, '13:25:00', '14:25:00');
/*!40000 ALTER TABLE `lessons` ENABLE KEYS */;

-- Dumping structure for tabell vklass2.0.rooms
CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  UNIQUE KEY `name` (`name`(100)),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumpar data för tabell vklass2.0.rooms: ~1 rows (approximately)
DELETE FROM `rooms`;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` (`id`, `name`) VALUES
	(1, '3612');
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
