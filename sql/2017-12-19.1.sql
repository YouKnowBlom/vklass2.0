-- --------------------------------------------------------
-- Värd:                         127.0.0.1
-- Server version:               10.1.25-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for vklass2.0
CREATE DATABASE IF NOT EXISTS `vklass2.0` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `vklass2.0`;

-- Dumping structure for tabell vklass2.0.classes
CREATE TABLE IF NOT EXISTS `classes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `lessons` text NOT NULL,
  `courses` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`(100))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumpar data för tabell vklass2.0.classes: ~1 rows (approximately)
DELETE FROM `classes`;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
INSERT INTO `classes` (`id`, `name`, `lessons`, `courses`) VALUES
	(1, 'TE417', '[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]', '[1,2,3,4,5,6,7,8,9]');
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;

-- Dumping structure for tabell vklass2.0.courses
CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `shortName` text NOT NULL,
  `teacher` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_courses_teatchers` (`teacher`),
  CONSTRAINT `FK_courses_teatchers` FOREIGN KEY (`teacher`) REFERENCES `teachers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumpar data för tabell vklass2.0.courses: ~9 rows (approximately)
DELETE FROM `courses`;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` (`id`, `name`, `shortName`, `teacher`) VALUES
	(1, 'Datalagring', 'Data', 1),
	(2, 'Examensarbete VI', 'Ex Arbete', 2),
	(3, 'Gränssnittsdesign', 'Gräs', 2),
	(4, 'Gymnasieingenjören i praktiken', 'Gymn Ingen', 5),
	(5, 'Mentorstid', 'Mentorstid', 2),
	(6, 'Mjukvarudesign', 'Mjukdesign', 1),
	(7, 'Mobila applikationer', 'Mob App', 1),
	(8, 'Programmering 2', 'Prog 2', 3),
	(9, 'Webbutveckling 2', 'Webb 2', 4);
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;

-- Dumping structure for tabell vklass2.0.days
CREATE TABLE IF NOT EXISTS `days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumpar data för tabell vklass2.0.days: ~5 rows (approximately)
DELETE FROM `days`;
/*!40000 ALTER TABLE `days` DISABLE KEYS */;
INSERT INTO `days` (`id`) VALUES
	(1),
	(2),
	(3),
	(4),
	(5);
/*!40000 ALTER TABLE `days` ENABLE KEYS */;

-- Dumping structure for tabell vklass2.0.lessons
CREATE TABLE IF NOT EXISTS `lessons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course` int(11) NOT NULL,
  `room` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `startTime` time NOT NULL DEFAULT '00:00:00',
  `endTime` time NOT NULL DEFAULT '00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_lession_courses` (`course`),
  KEY `FK_lession_rooms` (`room`),
  KEY `FK_lessons_days` (`day`),
  CONSTRAINT `FK_lession_courses` FOREIGN KEY (`course`) REFERENCES `courses` (`id`),
  CONSTRAINT `FK_lession_rooms` FOREIGN KEY (`room`) REFERENCES `rooms` (`id`),
  CONSTRAINT `FK_lessons_days` FOREIGN KEY (`day`) REFERENCES `days` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumpar data för tabell vklass2.0.lessons: ~18 rows (approximately)
DELETE FROM `lessons`;
/*!40000 ALTER TABLE `lessons` DISABLE KEYS */;
INSERT INTO `lessons` (`id`, `course`, `room`, `day`, `startTime`, `endTime`) VALUES
	(1, 8, 1, 1, '08:20:00', '09:20:00'),
	(2, 4, 1, 1, '10:00:00', '10:55:00'),
	(3, 7, 1, 1, '11:05:00', '12:00:00'),
	(5, 6, 1, 1, '12:40:00', '13:35:00'),
	(6, 9, 1, 1, '14:00:00', '14:55:00'),
	(7, 2, 1, 2, '09:40:00', '10:35:00'),
	(8, 7, 1, 2, '11:15:00', '12:15:00'),
	(9, 5, 1, 2, '13:00:00', '13:30:00'),
	(10, 3, 1, 2, '13:40:00', '14:40:00'),
	(11, 1, 1, 2, '14:50:00', '15:45:00'),
	(12, 6, 1, 3, '08:20:00', '09:20:00'),
	(14, 2, 1, 3, '09:30:00', '10:30:00'),
	(15, 3, 1, 3, '10:40:00', '11:35:00'),
	(16, 1, 1, 4, '08:20:00', '09:20:00'),
	(17, 4, 1, 4, '11:45:00', '12:45:00'),
	(18, 4, 1, 5, '08:20:00', '09:15:00'),
	(19, 8, 1, 5, '11:10:00', '12:05:00'),
	(20, 9, 1, 5, '13:25:00', '14:25:00');
/*!40000 ALTER TABLE `lessons` ENABLE KEYS */;

-- Dumping structure for tabell vklass2.0.news
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uploader` int(11) NOT NULL,
  `title` text NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_news_students` (`uploader`),
  CONSTRAINT `FK_news_students` FOREIGN KEY (`uploader`) REFERENCES `students` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumpar data för tabell vklass2.0.news: ~16 rows (approximately)
DELETE FROM `news`;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` (`id`, `dateTime`, `uploader`, `title`, `text`) VALUES
	(1, '2017-10-26 11:11:42', 3, 'Initial Commit', 'Av: Andreas Blomqvist'),
	(2, '2017-11-10 11:24:02', 2, 'Fonter', 'Av: Axel Hedvall'),
	(3, '2017-11-10 11:37:13', 2, 'Fixade brutna konts', 'Av: Axel Hedvall'),
	(4, '2017-11-13 11:38:14', 3, 'Tog bort index.php', 'Av: Andreas Blomqvist'),
	(5, '2017-11-17 11:42:39', 2, 'lade till schema', 'Av: Axel Hedvall'),
	(6, '2017-12-08 11:44:18', 2, 'schema.php', 'Av: Axel Hedvall'),
	(7, '2017-12-11 11:46:42', 2, 'schema.php klart', 'Av: Axel Hedvall'),
	(8, '2017-12-11 11:47:38', 3, 'Tog bort scripts.js', 'Av: Andreas Blomqvist'),
	(9, '2017-12-11 11:49:08', 2, 'Lade till courses.php', 'Av: Axel hedvall'),
	(10, '2017-12-11 11:50:10', 3, 'Ändrade filnamn', 'Av: Andreas Blomqvist'),
	(11, '2017-12-11 11:50:36', 2, 'Började på courses.php', 'Av: Axel Hedvall'),
	(12, '2017-12-11 11:52:20', 3, 'Nya animationer', 'Av: Andreas Blomqvist'),
	(13, '2017-12-13 11:55:01', 2, 'Fixade länkar', 'Av: Axel Hedvall'),
	(14, '2017-12-13 11:55:31', 2, 'Lade till nyheter-sida', 'Av: Axel Hedvall'),
	(15, '2017-12-15 11:57:02', 3, 'Raderade scripts.js', 'Nu så raderade Andreas scripts.js'),
	(16, '2017-12-15 11:58:56', 3, 'Fixade animationer', 'Andreas fixade lite animationer och sånt');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;

-- Dumping structure for tabell vklass2.0.rooms
CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`(100))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumpar data för tabell vklass2.0.rooms: ~1 rows (approximately)
DELETE FROM `rooms`;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` (`id`, `name`) VALUES
	(1, '3612');
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;

-- Dumping structure for tabell vklass2.0.students
CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namn` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `class` int(11) NOT NULL,
  `fullname` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`namn`(100)),
  KEY `FK__classes` (`class`),
  CONSTRAINT `FK_students_classes` FOREIGN KEY (`class`) REFERENCES `classes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='studenter';

-- Dumpar data för tabell vklass2.0.students: ~4 rows (approximately)
DELETE FROM `students`;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` (`id`, `namn`, `email`, `password`, `class`, `fullname`) VALUES
	(1, 'jesperholm98', 'jesper.holm@skola.taby.se', '$2y$12$c.4ex6SbYugUP01SYWZ5qug7EwUjHNoEi1E9n3rVWk1kEMjJlvkhG', 1, 'Jesper Holm'),
	(2, 'axelhedvall98', 'axel.hedvall@skola.taby.se', '$2y$12$7xmmjeW45fSyJU.4ytYOM.W75C0I5mhx7r4Wf0vgMQvYQHmrTQ4Ue', 1, 'Axel Hedvall'),
	(3, 'andreasblomqvist98', 'andreas.blomqvist@skola.taby.se', '$2y$12$ZNhl0AyniJIoZpmCcPByTu1WosA9X9f9Da9QxRUCQda/u9mwWNWCS', 1, 'Andreas Blomqvist'),
	(4, 'a', 'a', '$2y$12$omdMtWJNMYFHXqTUoRYvoOSOaZO0b3FbPbrEpTl/IBPVL/VTweC1e', 1, 'a');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;

-- Dumping structure for tabell vklass2.0.teachers
CREATE TABLE IF NOT EXISTS `teachers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`(100))
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumpar data för tabell vklass2.0.teachers: ~5 rows (approximately)
DELETE FROM `teachers`;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
INSERT INTO `teachers` (`id`, `name`) VALUES
	(1, 'Arif Khalifa'),
	(2, 'Björn Larsson'),
	(3, 'Gösta Körlof'),
	(4, 'Holger Rosencrantz'),
	(5, 'Nils Norberg');
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
