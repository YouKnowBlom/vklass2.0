<?php session_start(); if($_SESSION["userType"] != 2){header("location:./index.php");}?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Teacher</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="http://www.chartjs.org/dist/2.7.1/Chart.bundle.js"> </script>
	<script src="http://www.chartjs.org/samples/latest/utils.js"> </script>
</head>
<style>
	#setAttendance {
		position: absolute;
		top: 10%;
		bottom: 10%;
		left: 20%;
		right: 20%;
		background-color: white;
		box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
		border-radius: 1em;
		padding: 1em;
		<?php if(!isset($_GET["attendance"])) { echo "display: none;";} ?>
	}
	#setAttendance * {
		color: black;
	}
	
	#setAttendance table {
		width: 60%;
		margin: auto;
	}
		
	#saveAttendance {
		line-height: 3em;
		box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
		position: absolute;
		bottom: 1em;
		border-radius: 1em;
		width: 8em;
		text-align: center;
		left: calc(50% - 4em);
		cursor: pointer;
		text-decoration:none;
	}
	
	td.attendanceButtons {
		box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
		border-radius: 1em;
		width: 15.25em;
	}
	
	label.attendanceButton {
		display: inline-block;
		/*position: relative;
		padding-left: 4.5em;
		margin-bottom: 2.25em;*/
		cursor: pointer;
		line-height: 3em;
		height: 3em;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}
	
	label.attendanceButton input {
	  	position: absolute;
	  	opacity: 0;
    	cursor: pointer;
	}
	
	.radio {
	  	/*position: absolute;
		width: 100%;
		top: 0;
		left: 0;*/
    	background-color: #eee;
		text-align: center;
		padding: 1em;
	}
	
	.attendant {
		border-radius: 1em 0 0 1em;
	}
	.invalidAbsent {
		border-radius: 0 1em 1em 0;
	}
	
	label.attendanceButton:hover input ~ .radio {
		background-color: #ccc;
	}	
	
	label.attendanceButton input:checked ~ .attendant {
		background-color: lime;
	}
	label.attendanceButton input:checked ~ .absent {
		background-color: yellow;
	}
	label.attendanceButton input:checked ~ .invalidAbsent {
		background-color: red;
	}
	
	/*
	label.attendanceButton input:checked ~ .radio {
    	background-color: #2196F3;
	}*/
	
	.radio:after {
		content: "";
		position: absolute;
		display: none;
	}
	
	label.attendanceButton:hover input:checked ~ .radio:after {
		display: inline-block;
	}
	#canvas{
		width: 70% !important; 
		height: 70% !important;
		margin: 0 auto;
		background-color: white;
	}
</style>
<body style="height: 100vh; background:#F3AD71; margin: 0;">
		<?php
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "vklass2.0";
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            } 
            $conn->query("SET NAMES utf8");
            
            $classid = $_SESSION["classID"];
            $sql = "SELECT * FROM students WHERE class=$classid AND type=1";
            $result = $conn->query($sql);
    
            $students = [];
            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    $arr = array("id" => $row["id"], "name" => $row["fullname"]);
                    array_push($students, $arr);
                }
            } else {
                //echo "~0 results";
            }
			
			$class = [];
			$attendanceWeek = 0;
			$attendancelesson = 0;
			if(isset($_GET["attendance"])) {
				$attendancelesson = $_GET["id"];
				$attendanceWeek = $_GET["w"];
				
				$sql = "SELECT students.id, students.fullname FROM lessons INNER JOIN students ON lessons.class=students.class WHERE students.type=1 AND lessons.id=$attendancelesson ORDER BY students.fullname";
				$result = $conn->query($sql);
		
				if ($result->num_rows > 0) {
					while($row = $result->fetch_assoc()) {
						$arr = array("id" => $row["id"], "name" => $row["fullname"]);
						array_push($class, $arr);
					}
				} else {
					//echo "~0 results";
				}
			}			
			
			
			
			
            $conn->close();
        ?>
	<header>
		<a id="back" href="./index.php?from=teacher"><img src="img/arrow.svg" alt="back arrow"></a>
		<h1>TEACHER</h1>
	</header>
    <table>
			<!--<?php
				foreach($students as &$student) {
					$id = $student["id"];
					$name = $student["name"];
					echo "<a href=\"?id=$id\"><tr><td>$name</td></tr></a>";
				}
			?>-->
		</table>
        
        <div id="setAttendance" data-lessonid="<?php echo $attendancelesson; ?>" data-week="<?php echo $attendanceWeek; ?>">
        	<table>
            	<?php 
					if(isset($_GET["attendance"])) {
						foreach($class as &$student) {
							$id = $student["id"];
							$name = $student["name"];
							echo "<tr data-studentId='$id'><td>$name</td><td class='attendanceButtons'><label class='attendant attendanceButton'><input type='radio' name='$id' checked='checked' value='attendant'><span class='radio attendant'>Närvarao</span></label><label class='absent attendanceButton'><input type='radio' name='$id' value='absent'><span class='radio absent'>Giltig</span></label><label class='invalidAbsent attendanceButton'><input type='radio' name='$id' value='invalidAbsent'><span class='radio invalidAbsent'>Ogiltig</span></label></td></tr>";
						}
					}
				?>
            	<!--<tr data-studentId='1'><td>Axel Hedvall</td><td class='attendanceButtons'><label class='attendant attendanceButton'><input type='radio' name='student' checked='checked' value='attendant'><span class='radio attendant'>Närvarao</span></label><label class='absent attendanceButton'><input type='radio' name='student' value='absent'><span class='radio absent'>Giltig</span></label><label class='invalidAbsent attendanceButton'><input type='radio' name='student' value='invalidAbsent'><span class='radio invalidAbsent'>Ogiltig</span></label></td></tr>-->
            </table>
            <a href="setattendance.php" id="saveAttendance">Spara</a>
			<script src="js/SetAttendance.js"></script>
        </div>
			<canvas id="canvas">
	
	

<?php
 
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "vklass2.0";
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	} 
	$conn->query("SET NAMES utf8");
	
	$classID = $_SESSION["classID"];
	$sql = "select lessons.id, lessons.startTime, lessons.endTime FROM lessons WHERE lessons.class='$classID'";
	$result = $conn->query($sql);
	
	$lessons =[];
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$new = array("id" => $row["id"], "start" => strtotime($row["startTime"]), "end" => strtotime($row["endTime"]), "length" => round((strtotime($row["endTime"]) - strtotime($row["startTime"])) / 60, 2));
			array_push($lessons, $new);
		}
	} else {
		//echo "~0 results";
	}
	
	
	// jesper's pro kod för hämta elever och sätta deras närvaro på teacher grafen 
	
	
	$sql = "SELECT id,fullname FROM students WHERE type = 1 AND class = $classID ORDER BY fullname"; 
	$elevresults = $conn->query($sql);
	
	$students = array();
	if ($elevresults->num_rows > 0) {
		while($row = $elevresults->fetch_assoc()) {
			$students[$row["id"]] = array("id" => $row["id"], "name" => $row["fullname"], "invalid" => array(), "valid" => array(), "invalidTime" => 0, "validTime" => 0);
			
		}
	} else {
		//echo "~0 results";
	}
	
	$sql = "SELECT students.id AS 'studentid', students.fullname, attendance.type, attendance.week, lessons.id, courses.name, lessons.startTime, lessons.endTime FROM students
INNER JOIN attendance ON attendance.student = students.id
INNER JOIN lessons ON attendance.lesson = lessons.id
INNER JOIN courses ON lessons.course = courses.id
WHERE students.class = $classID";

$result = $conn->query($sql);

	$studentsinfo = array();
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$new = array("studentid" => $row["studentid"], "elev" => $row["fullname"], "type" => $row["type"], "id" => $row["id"], "course" => $row["name"], "week" => $row["week"], "startTime" => $row["startTime"], "endTime" => $row["endTime"]);
			array_push($studentsinfo, $new);
		}
	} else {
		//echo "~0 results";
	}
	
	$sql = "select lessons.id, lessons.startTime, lessons.endTime FROM lessons WHERE lessons.class='$classID'";
	$result = $conn->query($sql);
	
	$lessons =[];
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$new = array("id" => $row["id"], "start" => strtotime($row["startTime"]), "end" => strtotime($row["endTime"]), "length" => round((strtotime($row["endTime"]) - strtotime($row["startTime"])) / 60, 2));
			
			array_push($lessons, $new);
		}
	} else {
		//echo "~0 results";
	}
	
	$totalTime = 0;
	foreach($lessons as $lesson) {
		$totalTime += $lesson["length"];
	}
	$totalTime *= 4;
	
	foreach($students as &$student){
		foreach($studentsinfo as &$attendance){
			if($attendance["studentid"] == $student["id"]){
				if($attendance["type"] == 1){
					$length = round((strtotime($attendance["endTime"]) - strtotime($attendance["startTime"])) / 60, 2);
					$new = array("type" => $attendance["type"],"length" => $length, "startTime" => $attendance["startTime"], "endTime" => $attendance["endTime"] ,"week" => $attendance["week"],"lessonid" => $attendance["id"], "lessonname" => $attendance["course"]);
					array_push($student["invalid"], $new);
					$student["invalidTime"] += $new["length"];
				} else if ($attendance["type"] == 2){
					$length = round((strtotime($attendance["endTime"]) - strtotime($attendance["startTime"])) / 60, 2);
					$new = array("type" => $attendance["type"],"length" => $length, "startTime" => $attendance["startTime"], "endTime" => $attendance["endTime"] ,"week" => $attendance["week"],"lessonid" => $attendance["id"], "lessonname" => $attendance["course"]);
					array_push($student["valid"], $new);
					$student["validTime"] += $new["length"];
				}
			}

		}
	$student["invalidpercentage"] = round($student["invalidTime"]/$totalTime * 100, 2);
	$student["validpercentage"] = round($student["validTime"]/$totalTime * 100, 2);
	
	}
	var_dump($students);
	
	
	
	
	//var_dump($elevresults);
	//var_dump($students);
	//var_dump($studentsinfo);
	$conn->close();
?>		
<script>
        var barChartData = {
            labels: [<?php foreach($students as &$student) {echo '"'.$student["name"] .'",';} ?>],
            datasets: [{
                label: 'Närvaro',
                backgroundColor: '#83E80C',
                data: [
					<?php foreach($students as &$student) {echo '"'.(100-$student["invalidpercentage"]-$student["validpercentage"]).'",';} ?>
                ]
            }, {
                label: 'Giltig frånvaro',
                backgroundColor: '#FFA300',
                data: [
					<?php foreach($students as &$student) {echo '"'.$student["validpercentage"] .'",';} ?>
                ]
            }, {
                label: 'Frånvaro',
                backgroundColor: '#E80C2F',
                data: [
					<?php foreach($students as &$student) {echo '"'.$student["invalidpercentage"] .'",';} ?>
                ]
            }]
		};
		
        window.onload = function() {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
					legend: {
						display: true,
						labels: {
							fontSize: 35
						}
					},
					title:{
						display:true,
						
						
					},
					tooltips: {
						intersect: false,
						titleFontSize: 25,
						bodyFontSize: 25,
						mode: 'label',
						callbacks: {
							label: function (tooltipItems, data) {
								return tooltipItems.yLabel + '%' ;
							}
						}
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
							ticks: {
								fontSize: 15,
							}
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }
            });
        };
       
    </script>
	</canvas>
</body>
</html>