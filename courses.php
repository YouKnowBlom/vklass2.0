<?php header("location: ./index.php?from=courses") ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Kurser</title>

<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<style>
	body{
		height: 100vh; background:#ef85a4; margin: 0;
	}
	table {
		margin: auto;
		width: 60%;
		font-size: 200%;
	}
	td:nth-child(2) {
		text-align: right;
	}
</style>
<body>
	<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "vklass2.0";
		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 
		$conn->query("SET NAMES utf8");
		$sql = "SELECT * FROM classes WHERE name='TE417'";
		$result = $conn->query($sql);

		$courses =[];
		if ($result->num_rows === 1) {
			$row = $result->fetch_assoc();
			$class = array("id" => $row["id"], "name" => $row["name"], "lessons" => json_decode($row["lessons"]), "courses" => json_decode($row["courses"]));
			
			foreach($class["courses"] as &$course){
				$sql = "SELECT courses.name, teachers.name AS teacher FROM courses INNER JOIN teachers ON courses.teacher = teachers.id  WHERE courses.id=$course";
				$result = $conn->query($sql);
				
				if ($result->num_rows === 1) {
					$row = $result->fetch_assoc();
					array_push($courses, array("id" => $course, "name" => $row["name"], "teacher" => $row["teacher"]));
				} else {
					//echo "~0 results";
				}
			}
		} else {
			//echo "~0 results";
		}
		$conn->close();
	?>
	<header>
	<a id="back" href="./index.php?from=courses"><img src="img/arrow.svg" alt="back arrow"></a>
	<h1>KURSER</h1>
</header>
		<table>
			<?php
				foreach($courses as &$course) {
					$id = $course["id"];
					$name = $course["name"];
					$teacher = $course["teacher"];
					echo "<a href=\"?id=$id\"><tr><td>$name</td><td>$teacher</tr></a>";
				}
			?>
		</table>
</body>
</html>