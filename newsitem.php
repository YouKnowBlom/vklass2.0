<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Nyheter</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<style>
	body{
		height: 100vh;
		background:-webkit-linear-gradient(#20b2aa,#30d1c8);
		margin: 0;
	}
	div {
		background: white;
		width: 80vw;
		min-height: 10em;
		margin: auto;
		border-radius: 3em;
		max-width: 75em;
		padding-bottom:1px;
	}
	#title {
		color: black;
		text-align: center;
		margin-top:0;
	}
	#text {
		color: black;
		margin: 1em;
	}
	#last {
		color: gray;
		margin: 0 1em;
		font-size: 150%;
	}
	#date {
		color: gray;
		float: right;
	}
</style>
<body>
	<?php
		if(!isset($_GET["id"])) {
			header("location:./news.php");	
		}
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "vklass2.0";
		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		$conn->query("SET NAMES utf8");
		$id = $_GET["id"];
		$sql = "SELECT news.id, news.dateTime, students.fullname, news.title, news.text FROM news INNER JOIN students ON news.uploader = students.id WHERE news.id=$id";
		$result = $conn->query($sql);

		$new = array();
		if ($result->num_rows !== 1) {
			header("location:./news.php");	
		} else {
			$row = $result->fetch_assoc();
			$new = array("id" => $row["id"], "date" => $row["dateTime"], "uploader" => $row["fullname"], "title" => $row["title"], "text" => $row["text"]);
		}
		$conn->close();
	?>
	<header>
		<a id="back" href="./news.php"><img src="img/arrow.svg" alt="back arrow"></a>
		<h1>NYHET</h1>
	</header>
	
	<div>
    	<br>
    	<p id="title"><?php echo $new["title"]; ?></p>
        <p id="text"><?php echo $new["text"]; ?></p>
        <br>
        <p id="last">Av: <?php echo $new["uploader"]; ?><span id="date"><?php echo $new["date"]; ?></span><p>
    </div>
</body>
</html>