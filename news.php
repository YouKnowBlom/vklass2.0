<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Nyheter</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<style>
	body{
		height: 100vh; background:#7ee2b4; margin: 0;
	}
	.new {
    background:  white;
    box-shadow: 0px 2px 7px -2px black;
    border-radius:  10px;
    padding: 10px;
	}
	.new span, .new p {
		color: black;
		font-size: 0.8em;
	}
	#newsDiv {
		margin: 0 auto;
		width: 60%;
		font-size: 200%;
		overflow-y: scroll;
		height: 80vh;
		padding: 0 1em;
	}
	#newsDiv > a {
		text-decoration: none;
	}
	#newsDiv p {
		font-size: 0.6em;
	}
	.date {
		float: right;
	}
	@media only screen and (max-width: 500px) {
		#newsDiv {
			width: 100vw;
			height: calc(100vh - 4em);
			padding: 0;
		}
		h1 {
			padding: 0;
			font-size: 10vh;
		}
		header img {
			display: none;
		}
		.date {
			font-size: 50%;
		}
	}
</style>
<body>
	<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "vklass2.0";
		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 
		$conn->query("SET NAMES utf8");
		$sql = "SELECT * FROM news ORDER BY dateTime DESC";
		$result = $conn->query($sql);

		$news =[];
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$new = array("id" => $row["id"], "date" => $row["dateTime"], "title" => $row["title"], "text" => $row["text"]);

				array_push($news, $new);
			}
		} else {
			//echo "~0 results";
		}
		$conn->close();
	?>
	<header>
		<a id="back" href="./index.php?from=news"><img src="img/arrow.svg" alt="back arrow"></a>
		<h1>NYHETER</h1>
	</header>
	<div id="newsDiv">
		<?php
			foreach($news as &$new) {
				$id = $new["id"];
				$date = substr($new["date"], 0, strrpos($new["date"], ':'));
				$title = $new["title"];
				$text = $new["text"];
				echo "<a href=\"./newsitem.php?id=$id\"><div class=\"new\"><span>$title</span><span class=\"date\">$date</span><p>$text</p></div></a><br>";
			}
		?>
	</div>
</body>
</html>