<?php
	if(isset($_GET["id"]) && isset($_GET["w"])) {
		session_start();
	
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "vklass2.0";
		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 
		$conn->query("SET NAMES utf8");
		
		$id = $_GET["id"];
		$week = $_GET["w"];
		$user = $_SESSION["userid"];
		
		$sql = "SELECT id FROM cancelledlessons WHERE lessonid=$id AND week=$week"; //Check if lesson already is cancelled
		$result = $conn->query($sql);
		if ($result->num_rows === 1) {
			$sql = "DELETE FROM cancelledlessons WHERE lessonid=$id AND week=$week"; //Uncancel the lesson if it's already cancelled

			if ($conn->query($sql) === TRUE) {
				echo "Record deleted successfully";
				header("Location: ./schedule.php");
				die();
			} else {
				echo "Error deleting record: " . $conn->error;
			}
		} else {
			$sql = "INSERT INTO cancelledlessons (lessonid, week, cancelledby) VALUES ('$id', '$week', '$user')";
			
			if ($conn->query($sql) === TRUE) {
				echo "New record created successfully";
				header("Location: ./schedule.php");
				die();
			} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
			}
			
		$conn->close();
		}
	} else {
		header("Location: ./schedule.php");
		die();
	}
?>
